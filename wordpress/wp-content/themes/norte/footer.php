<footer>
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<p class="footer_title">TÉRMINOS DE USO</p>
				<ul>
					<li><a href="">NOVEDADES</a></li>
					<li><a href="">POLITICA</a></li>
					<li><a href="">LOCAL</a></li>
					<li><a href="">MUNDO</a></li>
					<li><a href="">TECH</a></li>
					<li><a href="">TURISMO</a></li>
				</ul>
				<br>
				<br>
			</div>
			<div class="col-md-4">
				<p class="footer_title">SOBRE NOSOTROS</p>
				<img src="<?php bloginfo('template_url'); ?>/img/logo1.png" alt="logo El Norte Hoy">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur vel sint perspiciatis nemo assumenda autem, ab error distinctio minus explicabo, pariatur.</p>
				<br>
				<br>
			</div>
			<div class="col-md-4 footer-contacto">
				<p class="footer_title">CONTACTO</p>
				<p><i class="fa fa-map-marker"></i><span> El Norte Hoy, periódico virtual de Gran Canaria</span></p>
				<p><i class="fa fa-phone"></i><span> 968 352 454</span></p>
				<p><i class="fa fa-mobile"></i><span> 676 788 154</span></p>
				<p><i class="fa fa-envelope-o"></i><span> info@elnortehoy.com</span></p>
			</div>
		</div>
	</div>
	<div class="footer">
		<div class="container">
			<p class="pull-left">El Norte Hoy 2015,Copyright | Todos los derechos</p>
			<p class="pull-right">Términos de uso. Diseñado y desarrollo por Eldista</p>
		</div>
	</div>
</footer>

<?php wp_footer(); ?>
<script src="<?php bloginfo('template_url'); ?>/js/swiper.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/bootstrap.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/main.js"></script>
</body>
</html>
