<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="<?php bloginfo('description'); ?>" />
	<title><?php bloginfo('name'); ?> <?php wp_title(); ?></title>
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,700|Roboto+Condensed:400,300|Lato' rel='stylesheet' type='text/css'>
	<link type="text/css" rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/bootstrap.min.css" media="screen" />
	<link type="text/css" rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/swiper.min.css" media="screen" />
	<link href='https://fonts.googleapis.com/css?family=Playfair+Display:400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<link href='https://fonts.googleapis.com/css?family=Cabin:400,700' rel='stylesheet' type='text/css'>
	<link type="text/css" rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/style.css" media="screen" />
	<link type="text/css" rel="stylesheet" href="<?php bloginfo( 'stylesheet_url' ); ?>" media="all" />
	<script src="<?php bloginfo('template_url'); ?>/js/jquery.js"></script>


	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div class="container">
		<div id="banner_1" class="banner">
			<?php
			wp_reset_query();
			if (have_posts()) :
				while (have_posts()) : the_post();
					?>
					<a href="<?php echo get_field('banner_1')['caption']; ?>" target="_blank">
						<img src="<?php echo get_field('banner_1')['sizes']['large']; ?>" alt="<?php echo get_field('banner_1')['alt'] ?>">
					</a>
					<?php
				endwhile;
			endif;
			?>
		</div>
		<header>
			<div class="row">
				<div class="col-xs-5">
					<a href="#" class="hidden-xs"> <i class="fa fa-envelope-o envelope"></i><br> SUSCRÍBETE </a>
					<div class="hidden visible-xs">
						<br>
						<br>
						<i class="fa fa-bars i_nav"></i><i class="fa fa-search i_nav"></i>
					</div>
				</div>
				<div class="col-xs-2 text-center">
					<a href="<?php get_home_url() ?>"> <img src="<?php bloginfo('template_url'); ?>/img/logo.png" class="logo" alt="El Norte Hoy logo"></a>
				</div>
				<div class="col-xs-5 text-right">
					<p class="actualizado">Actualizado <?php site_last_modified("H:i - l d/m/Y"); ?></p>
					<div class="social">
						<a href=""><i class="fa fa-facebook"></i></a>
						<a href=""><i class="fa fa-twitter"></i></a>
						<a href=""><i class="fa fa-google-plus"></i></a>
						<a href=""><i class="fa fa-youtube"></i></a>
					</div>
					<br>
					<a class="hidden-xs" href=""><span>EL NORTE HOY</span></a>
					<a class="hidden-xs" href=""><span>CONTACTO</span></a>
				</div>
			</div>
			<nav>
				<i class="fa fa-bars hidden-xs"></i><i class="fa fa-search hidden-xs"></i>
				<div class="menu hidden-xs"><?php wp_nav_menu( array( 'container_class' => 'hidden-xs',
					'menu' => 'principal' ) );
				?></div>
<!--				<ul class="text-center">-->
<!--					<li><a href="">PORTADA</a></li>-->
<!--					<li><a href="">ACTUALIDAD</a></li>-->
<!--					<li><a href="">LOCAL</a></li>-->
<!--					<li><a href="">POLÍTICA</a></li>-->
<!--					<li><a href="">ECONOMÍA</a></li>-->
<!--					<li><a href="">AGRICULTURA</a></li>-->
<!--					<li><a href="">TURISMO</a></li>-->
<!--				</ul>-->
				<div class="menu menu-mobile"><?php wp_nav_menu(array( 'container_class' => 'hidden visible-xs') );
				?></div>
			</nav>
		</header>
	</div>



