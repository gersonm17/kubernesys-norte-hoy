<?php get_header(); ?>

	<div class="container">
		<?php if ( have_posts() ) : ?>

			<?php if ( is_home() && is_front_page() ) : ?>
				<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
			<?php endif; ?>

			<?php
			while ( have_posts() ) :
				the_post();
			?>
				<div class="row">
					<div class="col-md-8">
						<h1><?php echo strip_tags(get_the_title());?></h1><br>
						<h4 class="date">Publicado el <?php the_time('l, j \d\e\ F'); ?></h4><br>
						<img src="<?php echo get_field('imagen_principal')['sizes']['large']; ?>" alt="<?php echo get_field('imagen_principal')['alt'] ?>"><br><br>
						<p><?php echo strip_tags(get_the_content(__('(more...)'))); ?></p>
					</div>
					<div class="col-md-4">
						<?php get_sidebar(); ?>
					</div>
				</div> <br> <br> <br>
			<?php endwhile;
		endif;
		?>
	</div>


<?php get_footer(); ?>