<?php get_header(); ?>

	<div class="container">
		<div id="principal_article">
			<?php
			wp_reset_query();
			query_posts('category_name=noticia_principal&posts_per_page=1&orderby=date');
			if (have_posts()) :
				while (have_posts()) : the_post();
					?>
					<a href="<?php the_permalink(); ?>">
						<img src="<?php echo get_field('imagen_principal')['sizes']['large']; ?>"
							 alt="<?php echo get_field('imagen_principal')['alt'] ?>">
						<div class="date">
							<span><?php the_time('l, j \d\e\ F'); ?></span>
						</div>
						<div class="txt_particle">
							<h3><?php the_title(); ?></h3>
							<p><?php the_content(); ?></p>
						</div>
					</a>
					<div class="buttons">
						<a href="">MUNDO</a href="">
						<a href="">LEER +</a>
					</div>
					<?php
				endwhile;
			endif;
			?>
		</div>
		<div id="banner_2" class="banner">
			<?php
			wp_reset_query();
			if (have_posts()) :
				while (have_posts()) : the_post();
					?>
					<a href="<?php echo get_field('banner_2')['caption']; ?>" target="_blank">
						<img src="<?php echo get_field('banner_2')['sizes']['large']; ?>"
							 alt="<?php echo get_field('banner_2')['alt'] ?>">
					</a>
					<?php
				endwhile;
			endif;
			?>
		</div>
	</div>
	<div id="content">
		<div class="container">
			<div class="row">
				<div class="col-md-4">

					<div class=""> <!-- contenedor noticias yy -->

						<?php
						wp_reset_query();
						query_posts('category_name=turismo&posts_per_page=2&orderby=date');

						if (have_posts()) :
							$countA = 0;
							while (have_posts()) : the_post();
								if ($countA == 0) {
									?>
									<a href="<?php the_permalink(); ?>">
										<div class="">
											<div class="image">
												<div class="layer"></div>
												<img
													src="<?php echo get_field('imagen_principal')['sizes']['medium']; ?>"
													alt="<?php echo get_field('imagen_principal')['alt'] ?>">
												<div class="etiqueta">MUNDO</div>
												<h3><?php the_title(); ?></h3>
											</div>
											<div class="share"><span>EL NORTE HOY</span><a href=""><i
														class="fa fa-share-alt-square"></a></i></div>
											<br><br>
										</div>
									</a>
								<?php } else {
									?>
									<a href="<?php the_permalink(); ?>">
										<div class="col-md-4">
											<img
												src="<?php echo get_field('imagen_principal')['sizes']['medium']; ?>"
												alt="<?php echo get_field('imagen_principal')['alt'] ?>">
										</div>
										<div class="col-md-8">
											<h3><?php the_title(); ?></h3>
											<?php the_content(false); ?>
										</div>

									</a>
									<div class="share col-md-12"><span>EL NORTE HOY</span><a href=""><i
												class="fa fa-share-alt-square"></a></i></div>
									<?php
								}
								$countA++;
							endwhile;
						endif;

						?>
					</div>
					<br> <!--end row-->
				</div>

				<div class="col-md-4 border-right">

					<div class=""> <!-- contenedor noticias xx -->

						<?php
						wp_reset_query();
						query_posts('category_name=turismo&posts_per_page=4&orderby=date');

						if (have_posts()) :
							$countA = 0;
							while (have_posts()) : the_post();
								if ($countA == 0) {
									?>

									<div>
										<a href="<?php the_permalink(); ?>">
											<div class="image">
												<div class="layer"></div>
												<img
													src="<?php echo get_field('imagen_principal')['sizes']['medium']; ?>"
													alt="<?php echo get_field('imagen_principal')['alt'] ?>">
												<div class="etiqueta">MUNDO</div>
												<h3><?php the_title(); ?></h3>
											</div>
										</a>

										<div class="share"><span>EL NORTE HOY</span>
											<a href=""><i class="fa fa-share-alt-square"></i></a>
										</div>
									</div>
									<br>
								<?php } else { ?>
									<div class="listitem-news row">
										<div class="col-md-3">
											<a href="<?php the_permalink(); ?>" class="thumb">
												<img src="<?php echo get_field('imagen_principal')['sizes']['medium']; ?>" alt="<?php echo get_field('imagen_principal')['alt'] ?>">
											</a>
										</div>
										<div class="col-md-9">
											<h4><a href="<?php the_permalink(); ?>"> <?php the_title(); ?> </a></h4>
											<div class="share share-right text-right">
												<a href="#"><i class="fa fa-share-alt-square"></i></a>
											</div>
										</div>
									</div>
									<hr class="hr-small">
								<?php }
								$countA++;
							endwhile;
						endif;
						?>
					</div>
					<br> <!--end row-->
				</div>
				<div class="col-md-4">
					<div id="banner_3">
						<?php
						wp_reset_query();
						if (have_posts()) :
							while (have_posts()) : the_post();
								?>
								<a href="<?php echo get_field('banner_3')['caption']; ?>" target="_blank">
									<img src="<?php echo get_field('banner_3')['sizes']['medium']; ?>"
										 alt="<?php echo get_field('banner_3')['alt'] ?>">
								</a>
								<?php
							endwhile;
						endif;
						?>
					</div>
					<br><br>
					<div class="row text-center"><!-- contenedor noticias zz -->
						<?php
						wp_reset_query();
						query_posts('category_name=politica&posts_per_page=3&orderby=date');
						if (have_posts()) :
							while (have_posts()) : the_post();
								?>
								<div class="col-md-4">
									<a href="<?php the_permalink(); ?>">
										<p class="etiqueta">MUNDO</p>
										<h4 style="margin: 10px 0 15px 0;"><?php the_title(); ?></h4>
										<img src="<?php echo get_field('imagen_principal')['sizes']['medium']; ?>"
											 alt="<?php echo get_field('imagen_principal')['alt'] ?>">
										<span class="small">El Norte Hoy.</span>
									</a>
								</div>
								<?php
							endwhile;
						endif;
						?>
					</div>
				</div>
			</div>
			<div id="banner_4" class="banner">
				<?php
				wp_reset_query();
				if (have_posts()) :
					while (have_posts()) : the_post();
						?>
						<a href="<?php echo get_field('banner_4')['caption']; ?>" target="_blank">
							<img src="<?php echo get_field('banner_4')['sizes']['large']; ?>"
								 alt="<?php echo get_field('banner_4')['alt'] ?>">
						</a>
						<?php
					endwhile;
				endif;
				?>
			</div>
		</div>
		<div class="bg_gray">
			<div class="container">
				<div class="row">
					<div class="col-md-8">
						<h3 class="title"><img class="icon" src="<?php bloginfo('template_url'); ?>/img/icon_b.png"
											   alt="icon"> POLITICA</h3>
						<div class="row"> <!-- contenedor noticias politica -->
							<?php
							wp_reset_query();
							query_posts('category_name=politica&posts_per_page=4&orderby=date');
							if (have_posts()) :
								while (have_posts()) : the_post();
									?>
									<div class="col-md-6">
										<a href="<?php the_permalink(); ?>">
											<div class="image">
												<div class="layer"></div>
												<img
													src="<?php echo get_field('imagen_principal')['sizes']['medium']; ?>"
													alt="<?php echo get_field('imagen_principal')['alt'] ?>">
												<h3><?php the_title(); ?></h3>
											</div>
										</a>

										<div class="share"><span>EL NORTE HOY</span><a href=""><i
													class="fa fa-share-alt-square"></a></i></div>
										<br><br>
									</div>
									<?php
								endwhile;
							endif;
							?>

						</div>
					</div>

					<div class="col-md-4 border-left">
						<h3 class="title"><img class="icon" src="<?php bloginfo('template_url'); ?>/img/icon_b.png"
											   alt="icon"> LOCAL</h3>
						<div class="row"> <!-- contenedor noticias local -->
							<?php
							wp_reset_query();
							query_posts('category_name=local&posts_per_page=2&orderby=date');
							if (have_posts()) :
								while (have_posts()) : the_post();
									?>
									<div class="col-md-12">
										<a href="<?php the_permalink(); ?>">
											<div class="image">
												<div class="layer"></div>
												<img
													src="<?php echo get_field('imagen_principal')['sizes']['medium']; ?>"
													alt="<?php echo get_field('imagen_principal')['alt'] ?>">
												<h3><?php the_title(); ?></h3>
											</div>
										</a>

										<div class="share"><span>EL NORTE HOY</span><a href=""><i
													class="fa fa-share-alt-square"></a></i></div>
										<br><br>
									</div>
									<?php
								endwhile;
							endif;
							?>

						</div>
					</div>
				</div>
				<div id="banner_5" class="banner">
					<?php
					wp_reset_query();
					if (have_posts()) :
						while (have_posts()) : the_post();
							?>
							<a href="<?php echo get_field('banner_5')['caption']; ?>" target="_blank">
								<img src="<?php echo get_field('banner_5')['sizes']['large']; ?>"
									 alt="<?php echo get_field('banner_5')['alt'] ?>">
							</a>
							<?php
						endwhile;
					endif;
					?>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-md-8 border-right">
					<h3 class="title">
						<img class="icon" src="<?php bloginfo('template_url'); ?>/img/icon_b.png" alt="icon"> VIDEO
					</h3>
					<div class="row"> <!-- contenedor noticias video -->
						<?php
						wp_reset_query();
						query_posts('category_name=video&posts_per_page=4&orderby=date');

						if (have_posts()) :
							$countA = 0;
							while (have_posts()) : the_post();
								if ($countA == 0) {
									?>
									<div class="col-md-12">
										<a href="<?php the_permalink(); ?>">
											<div class="image">
												<div class="layer"></div>
												<img
													src="<?php echo get_field('imagen_principal')['sizes']['medium']; ?>"
													alt="<?php echo get_field('imagen_principal')['alt'] ?>">
												<div class="etiqueta etiqueta_2">MUNDO</div>
												<div class="img_text">
													<h2><?php the_title(); ?></h2>
													<p><?php the_content(); ?></p>
												</div>
											</div>
										</a>
									</div>
									<div class="clearfix mb"></div>
								<?php } else {
									?>
									<div class="col-sm-4">
										<a href="<?php the_permalink(); ?>">
											<div class="image">
												<div class="layer"></div>
												<img
													src="<?php echo get_field('imagen_principal')['sizes']['medium']; ?>"
													alt="<?php echo get_field('imagen_principal')['alt'] ?>">
												<div class="etiqueta small_etiqueta">MUNDO</div>
												<h4><?php the_title(); ?></h4>
											</div>
											<br><br>
										</a>
									</div>
									<?php
								}
								$countA++;
							endwhile;
						endif;

						?>
					</div>
					<br> <!--end row-->
				</div>
				<div class="col-md-4">
					<h3 class="title"><img class="icon" src="<?php bloginfo('template_url'); ?>/img/icon_b.png"
										   alt="icon"> TURISMO</h3>
					<div class=""> <!-- contenedor noticias turismo -->

						<?php
						wp_reset_query();
						query_posts('category_name=turismo&posts_per_page=5&orderby=date');

						if (have_posts()) :
							$countA = 0;
							while (have_posts()) : the_post();
								if ($countA == 0) {
									?>

									<div class="">
										<a href="<?php the_permalink(); ?>">
											<div class="image">
												<div class="layer"></div>
												<img
													src="<?php echo get_field('imagen_principal')['sizes']['medium']; ?>"
													alt="<?php echo get_field('imagen_principal')['alt'] ?>">
												<div class="etiqueta">MUNDO</div>
												<h3><?php the_title(); ?></h3>
											</div>
										</a>

										<div class="share"><span>EL NORTE HOY</span><a href=""><i
													class="fa fa-share-alt-square"></a></i></div>
										<br>
									</div>
								<?php } else {
									?>
									<div class="listitem-news row">
										<div class="col-md-3">
											<a href="<?php the_permalink(); ?>" class="thumb">
												<img src="<?php echo get_field('imagen_principal')['sizes']['medium']; ?>" alt="<?php echo get_field('imagen_principal')['alt'] ?>">
											</a>
										</div>
										<div class="col-md-9">
											<h4><a href="<?php the_permalink(); ?>"> <?php the_title(); ?> </a></h4>
											<div class="share share-right text-right">
												<a href="#"><i class="fa fa-share-alt-square"></i></a>
											</div>
										</div>
									</div>
									<hr class="hr-small">
									<?php
								}
								$countA++;
							endwhile;
						endif;

						?>
					</div>
					<br> <!--end row-->
				</div>
			</div>
			<div id="banner_6" class="banner">
				<?php
				wp_reset_query();
				if (have_posts()) :
					while (have_posts()) : the_post();
						?>
						<a href="<?php echo get_field('banner_6')['caption']; ?>" target="_blank">
							<img src="<?php echo get_field('banner_6')['sizes']['large']; ?>"
								 alt="<?php echo get_field('banner_6')['alt'] ?>">
						</a>
						<?php
					endwhile;
				endif;
				?>
			</div>
		</div>
		<div class="bg_gray">
			<div class="container">
				<h3 class="title"><img class="icon" src="<?php bloginfo('template_url'); ?>/img/icon_b.png" alt="icon">
					MUNDO </h3>
				<div class="row"> <!-- contenedor noticias mundo -->
					<?php
					wp_reset_query();
					query_posts('category_name=mundo&posts_per_page=3&orderby=date');
					if (have_posts()) :
						while (have_posts()) :
							the_post();
							?>
							<div class="col-md-4">
								<a href="<?php the_permalink(); ?>" class="visible hidden-xs">
									<h2><?php the_title(); ?></h2>
									<div class="image image_bottom">
										<div class="layer"></div>
										<img src="<?php echo get_field('imagen_principal')['sizes']['medium']; ?>"
											 alt="<?php echo get_field('imagen_principal')['alt'] ?>">
										<div class="etiqueta">MUNDO</div>
									</div>
									<div class="item-content">
										<?php the_content(); ?>
									</div>
								</a>
								<a href="<?php the_permalink(); ?>" class="hidden visible-xs">
									<div class="row">
										<div class="col-xs-4">
											<div class="image image_bottom">
												<div class="layer"></div>
												<img
													src="<?php echo get_field('imagen_principal')['sizes']['medium']; ?>"
													alt="<?php echo get_field('imagen_principal')['alt'] ?>">
												<div class="etiqueta">MUNDO</div>
											</div>
										</div>
										<div class="col-xs-8">
											<h2><?php the_title(); ?></h2>
											<?php the_content(); ?>
										</div>
									</div>
								</a>
								<div class="share"><span>EL NORTE HOY</span><a href=""><i
											class="fa fa-share-alt-square"></a></i>  </div>
							</div>
							<?php
						endwhile;
					endif;
					?>
				</div><!-- End row -->
				<br><br>

			</div>


		</div>
		<div id="banner_7" class="banner">
			<?php
			wp_reset_query();
			if (have_posts()) :
				while (have_posts()) : the_post();
					?>
					<a href="<?php echo get_field('banner_7')['caption']; ?>" target="_blank">
						<img src="<?php echo get_field('banner_7')['sizes']['large']; ?>"
							 alt="<?php echo get_field('banner_7')['alt'] ?>">
					</a>
					<?php
				endwhile;
			endif;
			?>
		</div>
	</div> <!--end container-->
	</div> <!--end fondo-->
	<div class="container">
		<div class="etiquetas">
			<span>ETIQUETAS</span><br><br>
			<?php if (function_exists('wp_tag_cloud')) : ?>

				<!--					<h2>Popular Tags</h2>-->
				<ul>
					<li><?php wp_tag_cloud('smallest=8&largest=22'); ?></li>
				</ul>

			<?php endif; ?>
		</div>
	</div>
	</div>


<?php get_footer(); ?>