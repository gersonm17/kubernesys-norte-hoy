<?php $categoria=get_the_category();?>

<?php get_header(); ?>
<div class="container">
	<?php if ( have_posts() ) : ?>

		<?php if ( is_home() && is_front_page() ) : ?>
			<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
		<?php endif; ?>
	<div class="row">
		<?php
		while ( have_posts() ) :
			the_post();
			?>
			<div class="col-sm-7">
				<h1><?php echo strip_tags(get_the_title());?></h1><br>
				<h4 class="date">Publicado el <?php the_time('l, j \d\e\ F'); ?></h4><br>
				<img src="<?php echo get_field('imagen_principal')['sizes']['large']; ?>" alt="<?php echo get_field('imagen_principal')['alt'] ?>"><br><br>
				<p><?php echo strip_tags(get_the_content(__('(more...)'))); ?></p>
			</div><br><br>
		<?php endwhile;
	endif;
	?>
	<div class="col-sm-4 col-sm-offset-1">
		<br>
		<h4 style="margin:0" class="border-bottom"><b>Otras noticias</b></h4><br>
	</div>
	<div class="col-sm-4 col-sm-offset-1">
	<?php
			wp_reset_query();
			query_posts('category_name='.$categoria[0]->name);
//			query_posts('posts_per_page=5');
			if (have_posts()) :
				while (have_posts()) : the_post();
					?>
					<div class="row">
						<a href="<?php the_permalink(); ?>">
								<div class="col-xs-6">
									<div class="thumbnail_side">
										<img src="<?php echo get_field('imagen_principal')['sizes']['large']; ?>" alt="<?php echo get_field('imagen_principal')['alt'] ?>">
									</div>
								</div>
								<div class="col-xs-6">
									<br>
									<h5><?php echo strip_tags(get_the_title());?></h5>
								</div>
						</a>
					</div>
					<br>
					<?php
				endwhile;
			endif;
			?>
	</div>
	</div><br>
</div>
<?php get_footer(); ?>